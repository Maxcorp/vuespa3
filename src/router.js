import Vue from 'vue'
import Router from 'vue-router'
import store from './store.js'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Login from './components/auth/Login.vue'
import Register from './components/auth/Register.vue'
import Resource from './components/resources/Resources.vue'
import Blog from "@/views/Blog.vue";
import NotFound from "@/components/NotFound.vue";
import Post from "@/components/Post.vue";
import Contact from "@/components/Contact.vue";

Vue.use(Router)

let router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/resources',
            name: 'resources',
            component: Resource,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/about',
            name: 'about',
            component: About,
        },
        {
            path: "/blog",
            name: "blog",
            component: Blog,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/post/:id',
            name:'post',
            component: Post,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/contacts",
            name: "contacts",
            component: Contact,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/404",
            component: NotFound,
            meta: {
                title: "404"
            }
        },
        { path: "*", redirect: "/404" }
    ]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
})


export default router